const express = require("express");
const router = express.Router();
const siscotController = require("../controllers").siscot;

router.get(
  "/getSiscotiByDepartament/:id",
  siscotController.getSiscotiFromDepartament
);

router.get("/getSiscotiFromProiecte", siscotController.getSiscotiFromProiecte);
router.get("/", siscotController.getSiscoti);
module.exports = router;
